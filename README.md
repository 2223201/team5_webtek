# DIET ISABELO

Diet Isabelo™ is a web application that helps users make informed dietary choices when dining out at fast-food restaurants. It provides a platform for users to access calorie information for various menu items, set calorie goals, and order food while keeping track of their calorie intake. This README provides an overview of the application, its features, and how to use it.

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [Team Members](#team-members)

## Features

- **Calorie Information:** Access detailed calorie information for a wide range of fast-food menu items.
- **Calorie Goal Setting:** Set and track daily calorie goals for maintaining a balanced diet.
- **Food Ordering:** Build your food order and calculate its total calorie content.
- **Search Functionality:** Search for specific menu items, restaurants, or cuisines.
- **About the Developers:** Learn more about the team of developers behind the application.

## Getting Started

To use Diet Isabelo, follow these steps:

1. **Clone the Repository:** Clone this repository to your local machine using `git clone`.

2. **Allow file access to local resources:** 
- 1. Run cmd to get a command window
- 2. Move to the Chrome directory, `e.g., cd C:\Users\your-user-name\AppData\Local\Google\Chrome\Application`
- 3. Type: `chrome.exe --allow-file-access-from-files`

3. **Start the website** Open `index.html`


## Usage

- Launch the application by visiting the provided URL.
- Use the search feature to find menu items and view their calorie information.
- Set and track your daily calorie goals.
- Build and customize your food order while keeping an eye on the total calorie count.
- Visit the "About the Developers" page to learn more about the team.


## Team Members


- AGBAYANI, Rey John
- ANDAYA, Trisha
- CAMZA, Aeronn Charles
- DOMANTAY, Darren Franz
- ISABELO, Derek
- LO, Justin Louie
- REYES, Hans Lloyd